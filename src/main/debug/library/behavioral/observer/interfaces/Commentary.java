package library.behavioral.observer.interfaces;

public interface Commentary {
    void setDesc(String desc);
}
