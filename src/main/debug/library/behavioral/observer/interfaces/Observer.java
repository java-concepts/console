package library.behavioral.observer.interfaces;

 public interface Observer {
     void update(String desc);
     void subscribe();
     void unSubscribe();
     String getUserInfo();
}
