package library.behavioral.observer.impl;

import library.behavioral.observer.interfaces.Commentary;
import library.behavioral.observer.interfaces.Observer;
import library.behavioral.observer.interfaces.Subject;
import org.example.TestingClass;
import java.util.List;
import java.util.concurrent.TimeUnit;
public class CommentaryObject implements Commentary, Subject {
    private static final org.slf4j.Logger log =
            org.slf4j.LoggerFactory.getLogger(TestingClass.class);
    private List<Observer> observers = null;
    private String desc; // replace by a data object
    private String subjectDetails = null;
    public CommentaryObject(List<Observer> observers, String subjectDetails) {
        this.observers = observers;
        this.subjectDetails = subjectDetails;
        this.desc = "";
    }

    @Override
    public void subscribeObserver(Observer observer) {
        this.observers.add(observer);
        //log.info(this.subjectDetails);
    }

    @Override
    public void unSubscribeObserver(Observer observer){
        int index = observers.indexOf(observer);
        observers.remove(index);
    }

    @Override
    public void notifyObservers() {
        System.out.print("\n\nNotifying observers...\n");
        for(Observer observer: this.observers){
            observer.update(this.desc);
        }
    }

    @Override
    public String subjectDetails() {
        return this.subjectDetails;
    }

    @Override
    public void setDesc(String desc) {
        this.desc = desc;

        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println(desc);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void displayObservers(){
        System.out.println();
        for(Observer observer: this.observers){
            System.out.println(observer.getUserInfo());
        }
    }
}
