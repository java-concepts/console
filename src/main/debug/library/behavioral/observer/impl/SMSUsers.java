package library.behavioral.observer.impl;

import library.behavioral.observer.interfaces.Observer;
import library.behavioral.observer.interfaces.Subject;
import org.example.TestingClass;
import java.util.concurrent.TimeUnit;

public class SMSUsers implements Observer {
    private static final org.slf4j.Logger log =
            org.slf4j.LoggerFactory.getLogger(TestingClass.class);
    private Subject subject = null;
    private String userInfo;
    private String desc;

    public SMSUsers(Subject subject, String userInfo) {

        if(subject == null)
            throw  new IllegalArgumentException("No publisher found.");

        this.subject = subject;
        this.userInfo = userInfo;
        this.desc = "";
    }

    @Override
    public void update(String desc) {
        this.desc = desc;
        display();
    }

    @Override
    public void subscribe() {

        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            String display = String.format("Subscribing: %s to: %s", userInfo, subject.subjectDetails());
            System.out.println(display);
            this.subject.subscribeObserver(this);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unSubscribe() {

        try{
            TimeUnit.MILLISECONDS.sleep(1000);
            String display = String.format("Unsubscribing: %s to: %s", userInfo, subject.subjectDetails());
            log.debug(display);
            System.out.println(display);
            this.subject.unSubscribeObserver(this);
        }catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getUserInfo() {
        return this.userInfo;
    }

    public void display(){

        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            String display = String.format("userinfo: %s , details: %s, event: %s",
                    this.userInfo, this.subject.subjectDetails(), this.desc);
            //log.debug(display);

            System.out.println(display);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
