package library.stream;

import java.util.Arrays;
import java.util.List;

public class Streams {

  public void PeekSample(){
    List<String> list = Arrays.asList("hola","que" ,"tal");
    list.stream()
            .peek((stringPart)-> {
              System.out.println("***inicio****");
              System.out.println(stringPart);
              System.out.println("****fin inicio****");
            }).filter((stringPart)-> stringPart.length() > 0 )
            .peek((stringPart)-> {
              System.out.println("-----filtro--------");
              System.out.println(stringPart);
              System.out.println("-----fin filtro-----");
            }).map((stringPart)->stringPart.toUpperCase()).
            peek((stringPart)-> {
              System.out.println(">>>>>>mayusculas>>>>>>");
              System.out.println(stringPart);
              System.out.println(">>>>>>>fin mayusculas>>>>>>");
            }).forEach(System.out::println);
  }

}
