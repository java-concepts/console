package library.creational.builder.impl;

import library.creational.builder.interfaces.Builder;
import library.creational.builder.interfaces.Director;

public class DirectorImpl implements Director {
  @Override
  public void makeSUV(Builder builder) {
    builder.reset();
    builder.setSeats(5);
    builder.setEngine("Reanult");
    builder.setTripComputer();
    builder.setGPS();
  }

  @Override
  public void makeSportsCar(Builder builder) {
    builder.reset();
    builder.setSeats(2);
    builder.setEngine("V8");
  }
}
