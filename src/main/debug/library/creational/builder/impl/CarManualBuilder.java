package library.creational.builder.impl;

import library.creational.builder.entities.Manual;
import library.creational.builder.interfaces.Builder;

public class CarManualBuilder implements Builder {

  private Manual manual;

  public  CarManualBuilder(){
    this.reset();
  }

  @Override
  public void reset() {
    this.manual = new Manual();
    this.manual.setId("0");
    this.manual.setTitle("NewManual");
  }

  @Override
  public void setSeats(int number) {
    this.manual.setSeats(number);
  }

  @Override
  public void setEngine(String engine) {
    this.manual.setEngine(engine);
  }

  @Override
  public void setTripComputer() {
    this.manual.setHasTripComputer(true);
  }

  @Override
  public void setGPS() {
    this.manual.setHasGps(true);
  }

  public Manual getResult(){
    return this.manual;
  }
}
