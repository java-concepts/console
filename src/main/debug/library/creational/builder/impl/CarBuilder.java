package library.creational.builder.impl;

import library.creational.builder.entities.Car;
import library.creational.builder.interfaces.Builder;

public class CarBuilder implements Builder {

  private Car car;

  public CarBuilder()
  {
    this.reset();
  }

  @Override
  public void reset() {
    this.car.setBrand("");
    this.car.setPlate("");
    this.car.setModel("");
    this.car = new Car();
  }

  @Override
  public void setSeats(int number) {
    this.car.setSeats(number);
  }

  @Override
  public void setEngine(String engine) {
    this.car.setEngine(engine);
  }

  @Override
  public void setTripComputer() {
    this.car.setHasTripComputer(true);
  }

  @Override
  public void setGPS() {
    this.car.setHasGps(true);
  }

  public Car getResult(){
    return this.car;
  }
}
