package library.creational.builder.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Manual {
  public String id;
  public String title;
  public String engine;
  public int seats;
  public boolean hasTripComputer;
  public boolean hasGps;
}
