package library.creational.builder.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Car {
  public String model;
  public String brand;
  public String plate;
  public String engine;
  public long km;
  public int seats;
  public boolean hasTripComputer;
  public boolean hasGps;
}
