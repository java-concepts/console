package library.creational.builder.entities;


public class Computer {
  //required parameters
  private String HDD;
  private String RAM;

  //optional parameters
  private boolean isGraphicsCardEnabled;
  private boolean isBluetoothEnabled;
  private boolean isCameraIncluded;

  private Computer(ComputerBuilder computerBuilder){
    this.HDD = computerBuilder.HDD;
    this.RAM = computerBuilder.RAM;
    this.isBluetoothEnabled = computerBuilder.isBluetoothEnabled;
    this.isCameraIncluded = computerBuilder.isCameraIncluded;
    this.isGraphicsCardEnabled = computerBuilder.isGraphicsCardEnabled;
  }

  public  void getDetails(){
    String info = String.format("\nComputer => \nRAM: %s" +
            "\nHDD: %s" +
            "\nIs Bluetooth enabled?: %s" +
            "\nIs Camera included?  : %s" +
            "\nIs Graphics Card enabled?: %s",
            this.RAM,
            this.HDD,
            this.isBluetoothEnabled,
            this.isCameraIncluded,
            this.isGraphicsCardEnabled);
    System.out.println(info);
  }

  public static class ComputerBuilder
  {
    // required parameters
    private String HDD;
    private String RAM;

    // optional parameters
    private boolean isGraphicsCardEnabled;
    private boolean isBluetoothEnabled;

    private  boolean isCameraIncluded;

    public ComputerBuilder(String hdd, String ram){
      this.HDD=hdd;
      this.RAM=ram;
    }

    public ComputerBuilder setGraphicsCardEnabled(boolean isGraphicsCardEnabled) {
      this.isGraphicsCardEnabled = isGraphicsCardEnabled;
      return this;
    }

    public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
      this.isBluetoothEnabled = isBluetoothEnabled;
      return this;
    }

    public  ComputerBuilder setCameraIncluded(boolean isCameraIncluded) {
      this.isCameraIncluded = isCameraIncluded;
      return this;
    }

    public Computer build(){
      return new Computer(this);
    }

  }

}
