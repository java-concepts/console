package library.creational.builder.interfaces;

public interface Director {

  void makeSUV(Builder builder);

  void makeSportsCar(Builder builder);
}
