package org.example;

import library.behavioral.observer.impl.CommentaryObject;
import library.behavioral.observer.impl.SMSUsers;
import library.behavioral.observer.interfaces.Commentary;
import library.behavioral.observer.interfaces.Observer;
import library.behavioral.observer.interfaces.Subject;
import library.creational.builder.entities.Car;
import library.creational.builder.entities.Computer;
import library.creational.builder.entities.Manual;
import library.creational.builder.impl.CarBuilder;
import library.creational.builder.impl.CarManualBuilder;
import library.creational.builder.impl.DirectorImpl;
import library.creational.builder.interfaces.Director;
import library.stream.Streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestingClass {
    private static final org.slf4j.Logger log =
            org.slf4j.LoggerFactory.getLogger(TestingClass.class);
    public  static void TestObserver(){

        try{
            log.warn("Ejecución de Patron Observer (Behavioral)");

            Subject subject = new CommentaryObject(new ArrayList<>(), "Soccer Match 1234");
            Observer observer = new SMSUsers(subject, "Adam Warner [New York]");
            observer.subscribe();

            Observer observer2 = new SMSUsers(subject, "Tim Rooney [London]");
            observer2.subscribe();

            Observer observer3 = new SMSUsers(subject, "Laura Nielsen [Chicago]");
            observer3.subscribe();

            // notify all observers, before beginning events
            subject.notifyObservers();

            Commentary commentary = (Commentary) subject;
            System.out.println();
            commentary.setDesc("Welcome to live soccer match!");
            commentary.setDesc("Current score 0 - 0 ");

            observer2.unSubscribe();
            System.out.println();

            Observer observer4 = new SMSUsers(subject, "Kirk Collin [Aruba]");
            observer4.subscribe();
            System.out.println();

            commentary.setDesc("It's a goal!!");
            commentary.setDesc("Current score 1 - 0 ");
            commentary.setDesc("Half time. Current score: 1 - 0 ");

            // notify all observers, after events
            subject.notifyObservers();

            //subject.displayObservers();
        }catch (Exception e){
            //log.(e.getMessage() + "\n" + e.getStackTrace());
        }
    }

    public  static void TestPeek(){
        Streams streams = new Streams();
        streams.PeekSample();
    }

    public  static  void TestBuilder(){
        Computer computer =
                new Computer.ComputerBuilder("1 TB", "16 GB")
                        .setBluetoothEnabled(true)
                        .setCameraIncluded(true)
                        .setGraphicsCardEnabled(true).build();

        Computer computer2 =
                new Computer.ComputerBuilder("500 GB", "8 GB")
                        .setBluetoothEnabled(true)
                        .setCameraIncluded(true)
                        .setGraphicsCardEnabled(false).build();

        computer.getDetails();
        computer2.getDetails();
    }

    public static void TestBuilder2() {
        DirectorImpl director = new DirectorImpl();
        CarBuilder carBuilder = new CarBuilder();
        director.makeSportsCar(carBuilder);
        Car car = carBuilder.getResult();

        System.out.println("Car product =>");
        System.out.println(car.toString());

        CarManualBuilder manualBuilder = new CarManualBuilder();
        director.makeSportsCar(manualBuilder);
        Manual manual = manualBuilder.getResult();

        System.out.println("Manual product =>");
        System.out.println(manual.toString());

    }
}
